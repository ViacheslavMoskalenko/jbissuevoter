const wait = require('./wait');
class BasePage {
    constructor(browser) {
        this._browser = browser;
    }
    async close() {
        await this._page.close();
    }
    async wait(time) {
        await wait(time);
    }
    async getCurUrl() {
        return await this._page.url();
    }
    async scrollDown() {
        await this._page.evaluate(() => {
            window.scrollBy(0, document.body.scrollHeight);
        });
    }
}

module.exports = BasePage;