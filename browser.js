// docs can be found at https://pptr.dev/

const pptr = require('puppeteer');
let browser;

module.exports = async function() {
    if (!browser) {
        browser = await pptr.launch({
            defaultViewport: null,
            headless: false
        });
    }
    return browser;
};