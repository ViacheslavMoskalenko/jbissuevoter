const BasePage = require('./base-page');
class AuthPage extends BasePage {
    constructor(browser) {
        super(browser);
    }
    async open() {
        this._page = await this._browser.newPage();
        await this._page.goto('https://youtrack.jetbrains.com/issues');
        await this._page.waitForSelector('.loginButton_9d3');
        const loginBtn = await this._page.$('.loginButton_9d3');
        await loginBtn.click();
        await this._page.waitForSelector('.oauth-header__title');
    }
    async auth(user) {
        await this._fillUserName(user.nick);
        await this._fillPassword(user.password);
        await this._pressLogin();
    }
    async _fillUserName(userName) {
        const userNameField = await this._page.$('#username');
        await userNameField.type(userName);
    }
    async _fillPassword(password) {
        const passwordField = await this._page.$('#password');
        await passwordField.type(password);
    }
    async _pressLogin() {
        const loginBtn = await this._page.$('.auth-button.auth-button_wide');
        await loginBtn.click();
        await this._page.waitForSelector('[data-test="header-user-menu"]');
    }
}

module.exports = AuthPage;