// signup form: https://account.jetbrains.com/login?signup=
// login form: https://hub.jetbrains.com/auth/login?response_type
// the issue page: https://youtrack.jetbrains.com/issue/IDEA-188639
// temporary email service: https://dropmail.me/en/
// puppeteer docs: https://pptr.dev/#?product=Puppeteer&version=v1.7.0&show=outline

const logger = new (require('./logger'))('./log.txt');
const browser = require('./browser');

async function vote(user) {
    logger.log(`User data: ${user.nick} ${user.password}`);
    const browserInstance = await browser();
    const emailPage = new (require('./email.page'))(browserInstance);
    const signUpPage = new (require('./sign-up-page'))(browserInstance);
    const authPage = new (require('./auth-page'))(browserInstance);
    const issuePage = new (require('./issue-page'))(browserInstance);
    await emailPage.open();
    const userEmail = await emailPage.getEmail();
    user.email = userEmail;
    await signUpPage.open();
    try {
        await signUpPage.signOutIfNeeded();
        await signUpPage.signUp(user);
        logger.log(`User ${user.fullName} has been successfully sign up`);
    } catch (err) {
        logger.log(`Unable to sign up user ${user.fullName}`);
        if (err.captcha) {
            await emailPage.close();
        }
        throw err;
    } finally {
        logger.log('Closing sign up page');
        await signUpPage.close();
    }
    let newUserAccountPage = null;
    try {
        newUserAccountPage = await emailPage.confirmRegistration();
    } catch (err) {
        logger.log(`Unable to confirm user registration for user ${user.fullName}`);
        throw err;
    } finally {
        logger.log('Closing email page');
        await emailPage.close();
    }
    logger.log('Opening new user account page');
    await newUserAccountPage.open();
    try {
        logger.log('Filling in a user details');
        await newUserAccountPage.register(user);
    } catch (err) {
        logger.log(`Unable to fill account details for user ${user.fullName}`);
        throw err;
    } finally {
        logger.log('Closing new user account page');
        await newUserAccountPage.close();
    }
    
    logger.log('Opening auth page');
    await authPage.open();
    try {
        logger.log('Trying to authenticate a user');
        await authPage.auth(user);
    } catch (err) {
        logger.log(`Unable to authenticate user ${user.fullName}`);
        throw err;
    } finally {
        logger.log('Closing the auth page');
        await authPage.close();
    }
    logger.log('Opening the issue page');
    await issuePage.open();
    try {
        logger.log('Trying to upvote the issue');
        await issuePage.upvote();
        logger.log('Trying to logout');
        await issuePage.clickLogout();
    } catch (err) {
        logger.log(`Unable to upvote the issue using user ${user.fullName}`);
        throw err;
    } finally {
        logger.log('Closing the issue page');
        await issuePage.close();
    }
}

module.exports = vote;