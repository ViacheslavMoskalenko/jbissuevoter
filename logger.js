const path = require('path');
const fs = require('fs');
class Logger {
    constructor(logPath) {
        try {
            this._logFile = fs.openSync(path.join(__dirname, logPath), 'a');            
        } catch(err) {
            console.error('Unable to open log file using path');
            throw new Error(err);
        }
    }
    log(msg) {
        console.log(msg);
        try {
            fs.writeSync(this._logFile, `\n${msg}`);
        } catch(err) {
            console.warn('Failed to write data to log: ', err);
        }
    }
}

module.exports = Logger;