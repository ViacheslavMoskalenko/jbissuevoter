const vote = require('./vote');
const UserPool = require('./user-pool');
const userPool = new UserPool();
userPool.readFrom('./user-data.json');
(async () => {
    const browser = await require('./browser')();
    await userPool.forEach(vote);
    console.log('Closing a browser');
    await browser.close();
    console.log('Vote script is done');
})();