const BasePage = require('./base-page');
class NewUserAccountPage extends BasePage {
    constructor(browser, pageUrl) {
        super(browser);
        this._pageUrl = pageUrl;
    }
    async open() {
        this._page = await this._browser.newPage();
        await this._page.goto(this._pageUrl);
    }
    async register(user) {
        await this._fillData(user);
        await this._sumbit();
        await this._waitForRedirect();
    }
    async _waitForRedirect() {
        await this._page.waitForSelector('a[href="/profile-details"]');
        this.wait(5000);
    }
    async _sumbit() {
        const submitButton = await this._page.$('.pwd-submit-btn');
        await submitButton.click();
    }
    async _fillData(user) {
        await this._fillFirstName(user.firstName);
        await this._fillLastName(user.lastName);
        await this._fillUserName(user.nick);
        await this._fillPassword(user.password);
        await this._fillPasswordRepeat(user.password);
        await this._acceptAccountAgreement();
        await this._acceptDataAgreement();
    }
    async _fillFirstName(firstName) {
        const firstNameField = await this._page.$('#firstName');
        await firstNameField.type(firstName);
    }
    async _fillLastName(lastName) {
        const lastNameField = await this._page.$('#lastName');
        await lastNameField.type(lastName);
    }
    async _fillUserName(userName) {
        const userNameField = await this._page.$('#userName');
        await userNameField.type(userName);
    }
    async _fillPassword(password) {
        const passwordField = await this._page.$('#password');
        await passwordField.type(password);
    }
    async _fillPasswordRepeat(password) {
        const passwordConfirmField = await this._page.$('#pass2');
        await passwordConfirmField.type(password);
    }
    async _acceptAccountAgreement() {
        const privacyCheckbox = await this._page.$('input[type="checkbox"][name="privacy"]');
        await privacyCheckbox.click();
    }
    async _acceptDataAgreement() {
        const dataAgreementCheckbox = await this._page.$('input[type="checkbox"][name="mkt.newsletter.customer"]');
        await dataAgreementCheckbox.click();
    }
}

module.exports = NewUserAccountPage;