const BasePage = require('./base-page');
class IssuePage extends BasePage {
    constructor(browser) {
        super(browser);
    }
    async open() {
        this._page = await this._browser.newPage();
        await this._page.goto('https://youtrack.jetbrains.com/issue/IDEA-188639');
        try {
            console.log('Waiting for user consent');
            await this._page.waitForSelector('.content_b24 > div', {
                timeout: 10000
            });
            console.log('Trying to accept user concent modal');
            await this._page.evaluate(() => {
                const scrollArea = document.querySelector('.content_b24 > div');
                const scrollValue = document.querySelector('.markdown_57a').getBoundingClientRect().height;
                scrollArea.scrollTop = scrollValue;
            });
            console.log('Waiting for accept button');
            await this._page.waitForSelector('[data-test="accept"]')
            const acceptBtn = await this._page.$('[data-test="accept"]');
            console.log('Trying to accept user concent');
            await acceptBtn.click();
            await this._page.waitForSelector('.content_b24.contentWithTopFade_0e5.contentWithBottomFade_8b4 > div', {
                hidden: true
            });
            console.log('User concent has been successfully accepted');
        } catch (err) {
            console.warn('There were no user concent modal');
        }
        await this._page.waitForSelector('.yt-issue-body__summary.yt-issue-body__summary-common');
    }
    async upvote() {
        await this._page.waitForSelector('.yt-issue-view__body a.yt-issue-vote');
        const upVoteBtn = await this._page.$('.yt-issue-view__body a.yt-issue-vote');
        await this.wait(3000);
        await upVoteBtn.click();
        await this.wait(3000);
    }
    async clickLogin() {
        await this._page.waitForSelector('.loginButton_9d3');
        const loginBtn = await this._page.$('.loginButton_9d3');
        await loginBtn.click();
        await this._page.waitForSelector('.oauth-header__title');
    }
    async clickLogout() {
        const headerMenu = await this._page.$('[data-test="header-user-menu"]');
        await headerMenu.click();
        await this._page.waitForSelector('[data-test="ring-list-link"]:last-of-type');
        const logoutBtn = await this._page.$('[data-test="ring-list-link"]:last-of-type');
        await logoutBtn.click();
        await this._page.waitForSelector('.oauth-header__title');
        await this.wait(5000);
    }
}

module.exports = IssuePage;