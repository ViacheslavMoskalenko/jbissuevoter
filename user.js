class User {
    constructor({firstName, lastName, password}) {
        this._firstName = firstName;
        this._lastName = lastName;
        this._nick = '';
        this._email = '';
        this._password = password;
    }
    get nick() {
        return `${this._firstName.toLowerCase()}zz${this._lastName.toLowerCase()}`;
    }
    get password() {
        return this._password;
    }
    set email(value) {
        console.log('Setting new email: ', value);
        this._email = value;
    }
    get email() {
        return this._email;
    }
    get firstName() {
        return this._firstName;
    }
    get lastName() {
        return this._lastName;
    }
    get fullName() {
        return `${this._firstName} ${this._lastName}`;
    }
}

module.exports = User;