const BasePage = require('./base-page');
const NewUserAccountPage = require('./new-user-account.page');
class EmailPage extends BasePage {
    constructor(browser) {
        super(browser);
    }
    async open() {
        this._page = await this._browser.newPage();
        await this._page.goto('https://dropmail.me/en/')
    }
    async getEmail() {
        await this._page.waitForSelector('.email');
        return await this._page.evaluate(() => document.querySelector('.email').textContent.trim());
    }
    async confirmRegistration() {
        await this._page.waitForSelector('a[href^="https://account"]');
        const linkValue = await this._page.evaluate(() => document.querySelector('a[href^="https://account"]').href.trim());
        console.log('Confirmation link is: ', linkValue);
        return new NewUserAccountPage(this._browser, linkValue);
    }
}

module.exports = EmailPage;