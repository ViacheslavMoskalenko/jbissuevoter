const BasePage = require('./base-page');
class SignUpPage extends BasePage {
    constructor(browser) {
        super(browser);
    }
    async open() {
        this._page = await this._browser.newPage();
        await this._page.goto('https://account.jetbrains.com/login')
    }
    async signUp(user) {
        await this._fillUserEmail(user.email);
        await this.scrollDown();
        await this._signUp();
    }

    async _watchOutForCaptcha() {
        console.log('Waiting for captcha');
        await this.wait(3000);
        try {
            console.log('Searching captcha signs');
            const text = await this._page.$x("//*[contains(text(), 'not a robot')]");
            if (text.length > 0) {
                throw {
                    captcha: true
                };
            }
        } catch (err) {
            if (err.captcha) {
                console.log('Captcha found. Propagating error');
                throw err;
            }
            console.log('There were no captcha detected');
        }
    }
    
    async _fillUserEmail(userEmail) {
        const input = await this._page.$('#email');
        await input.type(userEmail); 
    }
    async signOutIfNeeded() {
        try {
            console.log('Trying to sign out if needed');
            await this._page.waitForSelector('.navbar-customer-name.dropdown', {
                timeout: 1e3
            });
            const panelLink = await this._page.$('.navbar-customer-name.dropdown .dropdown-toggle');
            await panelLink.click();
            await this._page.waitForSelector('a[href="/logout"]');
            const logOutLink = await this._page.$('a[href="/logout"]');
            console.log('Clicking sign out');
            await logOutLink.click();
            await this._page.waitForSelector('form[action="/authorize"]');
        } catch (err) {
            console.log('Can not sign out: ', err);
        }
    }
    async _signUp() {
        const button = await this._page.$('.eml-submit-btn');
        await button.click();
        await this._watchOutForCaptcha();
        await this._page.waitForSelector('.alert-success');
    }
}

module.exports = SignUpPage;