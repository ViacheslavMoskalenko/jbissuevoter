class Random {
    static generate(start = 0, end = 1) {
        return start + Math.floor(Math.random() * (end - start + 1));
    }
}

module.exports = Random;