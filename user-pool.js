const fs = require('fs');
const path = require('path');
const Random = require('./random');
const User = require('./user');

class UserPool {
    constructor() {
        this._pool = [];
    }
    _addToPool(user) {
        return this._pool.push(user);
    }
    _priorityAdd(user) {
        return this._pool.unshift(user);
    }
    _pickFromPool() {
        return this._pool.shift();
    }
    async forEach(cb = () => {}) {
        while (this._pool.length) {
            const user = this._pickFromPool();
            try {
                console.log('Preparing to execute cb');
                await cb(user);
                console.log('Successfully completed cb execution for: ', user);
            } catch (err) {
                console.warn('Catched error while executing cb for: ', err);
                if (err.captcha) {
                    console.warn('Detected captcha protection. Putting user back the beginning of a queue');
                    this._priorityAdd(user);
                }
                console.log('Proceeding...');
            }
        }
    }
    readFrom(filePath) {
        let data;
        try {
            data = JSON.parse(fs.readFileSync(path.join(__dirname, filePath)));
        } catch (err) {
            const msg = 'Unable to read users data from file';
            console.error(`${msg}: `, err);
            throw new Error(msg);
        }
        const {firstNames, lastNames} = data;
        if (firstNames.length !== lastNames.length) {
            throw new Error('First names list has not the same length as last names list');
        }
        while (firstNames.length) {
            const firstName = firstNames.splice(
                Random.generate(0, firstNames.length - 1),
                1
            )[0];
            const lastName = lastNames.splice(
                Random.generate(0, lastNames.length - 1),
                1
            )[0];
            const user = new User({
                firstName,
                lastName,
                password: `Z${firstName}_${lastName}81`
            });
            this._addToPool(user);
        }
    }
}

module.exports = UserPool;